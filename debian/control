Source: libvideo-info-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: non-free/perl
Testsuite: autopkgtest-pkg-perl
XS-Autobuild: yes
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl,
                     libclass-makemethods-perl,
                     libmp3-info-perl
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libvideo-info-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libvideo-info-perl.git
Homepage: https://metacpan.org/release/Video-Info

Package: libvideo-info-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libclass-makemethods-perl,
         libmp3-info-perl
Description: Perl module to examine video files
 Video::Info is a module for working with video files. It can open files and
 determine its most likely type, then extract information such as play length,
 bitrate, resolution, dimensions, etc.
 .
 Currently there is support for MPEG, AVI, ASF and RIFF. There is Quicktime
 support in the upstream module which has been disabled in Debian because
 there is no current package for openquicktime. If you would like to see
 support for this, please file a bug report.
